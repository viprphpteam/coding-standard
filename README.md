vipr/coding-standard
========================

The coding standard of the Vipr projects.

# Installation

```
$ composer require --dev vipr/coding-standard
```

# Configuration

## PHP CS fixer

1. create a `.php_cs` file referencing the `.php_cs.dist` template:

```php
# .php_cs

<?php

$config = require 'vendor/vipr/coding-standard/.php_cs.dist';

$config->setFinder(
    \PhpCsFixer\Finder::create()
        ->in([
            __DIR__ . '/src',
            __DIR__ . '/tests',
            __DIR__ . '/spec',
        ])
);

return $config;

```

2. add `.php_cs.cache` to your `.gitignore`.

# Usage

```
$ vendor/bin/qa_codequality.sh          ## scan and display code standard errors
$ vendor/bin/qa_codequality.sh --ci     ## to generate junit test reports
$ vendor/bin/qa_codequality.sh --fix    ## try to automatically correct issues
```