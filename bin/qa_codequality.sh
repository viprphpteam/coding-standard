#!/usr/bin/env sh

set -euo pipefail

DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

BIN_CS="${DIR}/../../vendor/bin/phpcs"
BIN_CBF="${DIR}/../../vendor/bin/phpcbf"
BIN_CSF="${DIR}/../../vendor/bin/php-cs-fixer"

LOG_DIR='test-reports'
CS_LOG_FILE="${DIR}/../../${LOG_DIR}/phpcs.xml"
CSF_LOG_FILE="${DIR}/../../${LOG_DIR}/phpcsf.xml"

CS_SCAN_FOLDERS="./src" ##  ./tests ./spec

FIX=false
CI=false

while [ $# -gt 0 ]; do
  case "$1" in
    --ci)
      CI=true
      ;;
    --fix)
      FIX=true
      ;;
  esac
  shift
done

if [ "${FIX}" = true ]; then
  ${BIN_CBF} ${CS_SCAN_FOLDERS} --standard=./vendor/vipr/coding-standard/ruleset.xml
  ${BIN_CSF} fix --config=".php_cs" --allow-risky=yes --verbose
else
  if [ "${CI}" = true ]; then
    ${BIN_CS} ${CS_SCAN_FOLDERS} --report-junit=${CS_LOG_FILE} --standard=./vendor/vipr/coding-standard/ruleset.xml
    ${BIN_CSF} fix --config=".php_cs" --allow-risky=yes --verbose --dry-run --format=junit > ${CSF_LOG_FILE}
  else
    ${BIN_CS} ${CS_SCAN_FOLDERS} --standard=./vendor/vipr/coding-standard/ruleset.xml
    ${BIN_CSF} fix --config=".php_cs" --allow-risky=yes --verbose --dry-run --format=txt
  fi
fi
